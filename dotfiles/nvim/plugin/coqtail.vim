let g:coqtail_nomap = 1

nnoremap <leader>os <cmd>CoqStart<cr>
nnoremap <leader>oq <cmd>CoqStop<cr>
nnoremap <leader>oi <cmd>CoqInterrupt<cr>
nnoremap <leader>oj <cmd>CoqNext<cr>
nnoremap <leader>ok <cmd>CoqUndo<cr>
nnoremap <leader>ol <cmd>CoqToLine<cr>
nnoremap <leader>ot <cmd>CoqToTop<cr>
nnoremap <leader>oG <cmd>CoqJumpToEnd<cr>
nnoremap <leader>oE <cmd>CoqJumpToError<cr>
nnoremap <leader>ogd <cmd>CoqGoToDef[!]<arg><cr>
nnoremap <leader>or <cmd>CoqRestorePanels<cr>
